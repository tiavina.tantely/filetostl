# fileToSTL
Takes a list of points (in order) and a height value from a text file as input and outputs each extruded polygon as an stl file

## Usage

```
python extrudeToSTL.py -f filename_of_points_list
```

## Example for the input data
```
2 0,0 1,0 1,1 0.5,1.5 0,1
2 0,0 0.5,-0.5 1,0 1.5,-0.5 1,1 0.5,1.5 0,1
```
