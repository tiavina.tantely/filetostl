import numpy as np
from stl import mesh
import math
from scipy.spatial import Delaunay
import argparse
import os.path
from os import path
from shapely import geometry
from tri import to_triangles


def add_dim_to_simplices(arr, dim):
    h_vect = np.array([0, 0, dim])
    a = []
    
    for t in arr:
        _t = []
        for i in t:
            _t.append([i[0], i[1], dim])
        a.append(_t)
    
    return np.array(a)

def add_tri(trilist, toadd):
    for t in toadd:
        trilist.append(t)

def getTriangleFromSquare(pt1, pt2, h):
    pt1 = np.array([pt1[0], pt1[1], 0])
    pt2 = np.array([pt2[0], pt2[1], 0])    
    pt3 = np.array([pt1[0], pt1[1], h])
    pt4 = np.array([pt2[0], pt2[1], h])

    t1 = np.array([pt1, pt2, pt3])
    t2 = np.array([pt2, pt4, pt3])

    return np.array([t1,t2])

def parseFile(path):
    my_list = []
    h = [] 

    with open(path) as f:
        lines = f.readlines()

        for line in lines:
            line = line.strip() 
            points = [] 

            if line:

                data = [item.strip() for item in line.split(' ')]

                h.append(float(data[0]))

                for i in range(1, len(data)):
                    points.append( [float(item) for item in  data[i].split(',')])
                my_list.append(points)
        
    return my_list, h

def extrude_to_stl(pl, hl):
    for n in range(0, len(h_list)):
        h = h_list[n]
        points = p_list[n]
        
        i = 0
        tri_list = []

        #tri = Delaunay(points)

        t = toTriangles(points)
        points = np.array(points)
        
        #pts = points[tri.simplices]
        
        pts = toTriangles(points)

        top_pts = add_dim_to_simplices(pts, h)
        bottom_pts = add_dim_to_simplices(pts, 0)

        add_tri(tri_list,top_pts)
        add_tri(tri_list,bottom_pts)

        j=0
        for j in range(0, (len(points)-1)):
            side_pts = getTriangleFromSquare(points[j], points[j+1], h)
            add_tri(tri_list, side_pts)

        side_pts = getTriangleFromSquare(points[j+1], points[0], h)
        add_tri(tri_list, side_pts)

        nt = switchAxes(tri_list)
        cube = mesh.Mesh(np.zeros(len(tri_list), dtype=mesh.Mesh.dtype))

        for tr in nt:
            cube.vectors[i] = tr
            i+=1

        cube.save('shape'+str(n)+'.stl')
        
def toTriangles(polygon):
    tri_list = []
    polygon_1 = geometry.Polygon(np.squeeze(polygon))
    polygon_1 = geometry.polygon.orient(polygon_1)
   
    tri = to_triangles(polygon_1)

    for t in tri:
        arr = np.array(t.exterior)
        arr = np.resize(arr, arr.size - 2)
        arr = np.reshape(arr,(3,2))
        tri_list.append(arr)

    return np.array(tri_list)


def switchAxes(tri_list):
    new_tri_list = []
    for t in tri_list:
        _t = []
        for c in t:
            _c = np.array([c[0], c[2], c[1]])
            _t.append(_c)
        new_tri_list.append(_t)
    return np.array(new_tri_list)

parser = argparse.ArgumentParser()                                               

parser.add_argument("--filename", "-f", type=str, required=False)
args = parser.parse_args()
file = "data.txt"


if args.filename:
    file = args.filename

if path.exists(file):
    p_list, h_list = parseFile(file)
    extrude_to_stl(p_list, h_list)
else:
    print("Invalid path")

